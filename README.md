# Import data in csv format to mysql database for Catalyst 
# author: John Yao
## This script is to etract user data in csv format and create table in database.

##Prerequisites
PHP7.0, php7.0-mysql,Composer

## Installation Guide

1. Please go to root directory of this project and run the command below in CLI to install a required composer dependency:

  ```
  composer update
  ```
Once this command finishes, it should be ready to go. 

## User Guide

1. Put the users.csv file in the same folder with user_upload.php.

2. Based on the assumptions in the document provided, there are three main usages.

	To create database and table only :

```
  	php user_upload.php --create_table --u=`user` --p=`password` --h=`localhost`
```

  To dry run without altering database (file name must be supplied):

```
  	php user_upload.php --dry_run -fusers.csv
```

  To run the full version:

```
  	php user_upload.php -fusers.csv --u=`user` --p=`password` --h=`localhost`
```

##Note

Please run  	
```
  	php user_upload.php --help 
```
 to see all options. 