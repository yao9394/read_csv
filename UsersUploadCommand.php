<?php

require __DIR__.'/vendor/autoload.php';
require __DIR__.'/csvHelper.php';
require __DIR__.'/userDb.php';

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputDefinition;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class UsersUploadCommand extends Command 
{
	protected function configure() {
		$this->setName('users:upload')
		->setDescription('Help text')
		->addOption('file', 'f',InputOption::VALUE_OPTIONAL, 'This is the name of the CSV to be parsed')
		->addOption('create_table', null, InputOption::VALUE_NONE, 'This will cause the MySQL users table to be built (and no further action will be taken)')
		->addOption('dry_run', null, InputOption::VALUE_NONE, 'This will be used with the --file directive in the instance that we want 
to run the script but not insert into the DB. All other functions will be executed, 
but the database won\'t be altered.')
		->addOption('u', null, InputOption::VALUE_OPTIONAL, 'Username')
		->addOption('h', null, InputOption::VALUE_OPTIONAL, 'Host')
		->addOption('p', null, InputOption::VALUE_OPTIONAL, 'Password');
	}

	protected function execute(InputInterface $input, OutputInterface $output) {

		$file = $input->getOption('file');
        $create_table = $input->getOption('create_table');
        $dry_run = $input->getOption('dry_run');
        $u = $input->getOption('u');
        $p = $input->getOption('p');
        $h = $input->getOption('h');
 
        if ($create_table == true && $dry_run == false && !empty($u) && !empty($p) && !empty($h)) {
	
			$userDb = new userDb($u, $p, $h, 'user_db', $output);
			$userDb->createDBandUserTable();     	

            return $output->writeln("<comment>Create Table Only</comment>");
        } else if ($dry_run == true && !empty($file) && $create_table == false) {
        	$csvHelper = new csvHelper($file, $output);
			$csvHelper->extractColumnNames();
        	$csvHelper->readRestOfLines();
			$csvHelper->insertUserDataDryRun();
        	$csvHelper->printUserData();

            return $output->writeln("<comment>Dry Run</comment>");
        } else if (!empty($file) && $create_table == false && !empty($u) && !empty($p) && !empty($h) && $dry_run == false) {
			$userDbcontext = new userDb($u, $p, $h, 'user_db', $output);
			$userDbcontext->createDBandUserTable();
			$csvHelper = new csvHelper($file, $output);
			$csvHelper->extractColumnNames();
			$csvHelper->readRestOfLines();
			$csvHelper->insertUserData($userDbcontext);
			$csvHelper->printUserData();
            return $output->writeln("<comment>Full Version</comment>");
        } else {
            $output->writeln("<error>Wrong Option Combinations!</error>");
            $output->writeln("<comment>If you try to connect database, username, password and host must be supplied.</comment>");
            $output->writeln("<comment>If you try to dry run, please supply csv file.</comment>");
        }
	}
}