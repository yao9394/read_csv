<?php 

require __DIR__.'/vendor/autoload.php';

use Symfony\Component\Console\Output\OutputInterface;

class csvHelper {

	private $fileName;
	private $output;
	private $columnNames;
	private $acceptedColumns = array('name', 'surname', 'email');
	private $allEmails;
	private $userData;

	public function __construct(string $fileName, OutputInterface $output) {
		$this->fileName = $fileName;
		$this->output = $output;
		$this->columnNames = array();
		$this->userData = array();
		$this->allEmails = array();
	}

	public function extractColumnNames() {
		$row = 1;
		if (($handle = fopen($this->fileName, "r")) !== false) {
			while (($data = fgetcsv($handle)) !== false && $row <= 1) {
				//Read the first line to extract column names only
				$num = count($data);
				$row++;
				for ($c=0; $c < $num; $c++) {
					array_push($this->columnNames, $this->removeSpace($data[$c]));
		        }
			}
		} else {
			$this->output->writeln('<error>Failed to open '.$this->fileName.'. Task aborted... </error>');
			exit;
		}

		if(!$this->validateColumns())  {
			$this->output->writeln('<error>Invalid data format - column names not accepted or not in order.</error>');
			$this->output->writeln('<error>Task aborted.</error>');
			exit;
		}
	}

	private function validateColumns() {
		return $this->columnNames === $this->acceptedColumns;
	}

	private function removeSpace($str) {
		return preg_replace('/\s/', '', $str);
	}

	public function readRestOfLines() {
		$row = 1;

		if (($handle = fopen($this->fileName, "r")) !== false) {

		    while (($data = fgetcsv($handle)) !== false) {
		        $num = count($data);
		        $row++;
		        if( $row > 2) {
					$user = array();
						for ($c=0; $c < $num; $c++) {
							switch ($c) {
							case 0:
								$user['name'] = $this->cleanNames($data[$c]);
								break;
							case 1:
								$user['surname'] = $this->cleanNames($data[$c]);
								break;
							case 2:
								$user['email'] = strtolower($data[$c]);
								array_push($this->allEmails, strtolower($data[$c]));
								break;
							default:
								break;
						}
					}
					array_push($this->userData, $user);
		        }

		    }
		    fclose($handle);
		}
	}

	public function insertUserData($userDb) {
		foreach ($this->userData as $key => $user) {
			if($this->validateEmail($user['email']) && !$this->checkDuplicatedEmail($user['email'], $key)) {
				$userDb->insertOneUser($user, $this->acceptedColumns);
			} else {
				$this->output->writeln('<error>Invalid value (invalid format or duplicated value). Insertion for '.$user['name'].' aborted </error>');
			}
		}
		mysqli_close($userDb->getLink());
	}

	public function insertUserDataDryRun() {
		foreach ($this->userData as $key => $user) {
			if($this->validateEmail($user['email']) && !$this->checkDuplicatedEmail($user['email'], $key)) {
				$this->output->writeln('<info>User '.$user['name'].' created successfully</info>');
			} else {
				$this->output->writeln('<error>Invalid value (invalid format or duplicated value). Insertion for '.$user['name'].' aborted </error>');
			}
		}
	}

	public function printUserData() {
		$this->output->writeln('');
		$this->output->writeln('');
		$this->output->writeln('<question>            User Data Summary            </question>');
		$this->output->writeln('<info>  name  |  surname  | email  |    message    </info>');
		$this->output->writeln('<info>-------------------------------------------------------</info>');
		foreach ($this->userData as $key => $user) {
			$this->output->writeln('  '.$user['name'].'  <info>|</info>  '.$user['surname'].'  <info>|</info> '.$user['email'].'  '.($this->validateEmail($user['email'])?'':'<error>Invalid Email Address</error>').($this->checkDuplicatedEmail($user['email'], $key)?' <error>Duplicated Email Address</error>':''));
		}
		$this->output->writeln('<info>-------------------------------------------------------</info>');
		$this->output->writeln('');
		$this->output->writeln('');
	}

	private function cleanNames($name) {
		$name = preg_replace('/[^a-z]/i', '', $this->removeSpace($name));
		return ucwords(strtolower($name));
	}

	private function cleanEmail($email) {
		$email = preg_replace('/[^a-z@.]/i', '', $this->removeSpace($email));
		return strtolower($email);
	}

	private function validateEmail($email) {
		if(filter_var($email, FILTER_VALIDATE_EMAIL) == $email) {
			return true;
		} else {
			return false;
		}
	}

	private function checkDuplicatedEmail($email ,$key) {
		if(array_count_values($this->allEmails)[$email] > 1 && array_search($email, $this->allEmails) != $key) {
			//Make sure it should not be considered as duplicated email for the first occurance.
			return true;
		} else {
			return false;
		}
	}
}