<?php 

require __DIR__.'/vendor/autoload.php';

use Symfony\Component\Console\Output\OutputInterface;

class userDb {

	private $username;
	private $password;
	private $host;
	private $db;
	private $output;
	private $link;
	
	public function __construct(string $username,string $password,string $host, string $db, OutputInterface $output) {
		$this->username = $username;
		$this->password = $password;
		$this->host = $host;
		$this->db = $db;
		$this->output = $output;
		$this->link = mysqli_connect($this->host, $this->username, $this->password);
	}

	//Check if the db exists
	public function checkDbConnection() {

		if (!$this->link) {
			$this->output->writeln('<error>Could not connect: ' .  mysqli_connect_error() .'</error>');
		    die('Could not connect: ' . mysqli_connect_error());
		}

		$select_db = mysqli_select_db($this->link, $this->db);

		if (!$select_db) {
		  	return false;
		} else {
			return true;
		}

		mysqli_close($this->link);

	}

	// Create DB accordingly
	public function createDb() {
		if($this->checkDbConnection() == false) {
			$sql = 'CREATE DATABASE '.$this->db . ' CHARACTER SET utf8 COLLATE utf8_general_ci';

		  	if (mysqli_query($this->link, $sql)) {
		      $this->output->writeln('<info>Database '.$this->db.' created successfully</info>');
		  	} else {
		      $this->output->writeln('<error>Error creating database: ' . mysqli_error($this->link) .'</error>');
		  	}

		  	mysqli_close($this->link);
		} else {
			$this->output->writeln('<info>Database '.$this->db .' created already.</info>');
		}
	}

	// Create table only, without any data
	public function createUserTable() {
		if($this->checkDbConnection() == true) {
			$dropSql = "DROP TABLE IF EXISTS users";

			if(mysqli_query($this->link, $dropSql)) {
				$this->output->writeln('<info>Deleted table ... </info>');
				$this->output->writeln('<info>Creating table ... </info>');
				$sql = "
				CREATE TABLE users (
					id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
					name VARCHAR(30) NOT NULL,
					surname VARCHAR(30) NOT NULL,
					email VARCHAR(50) NOT NULL,
					UNIQUE (email)
					)";
				if (mysqli_query($this->link, $sql)) {
					      $this->output->writeln('<info>User table created successfully</info>');
						} else {
					      $this->output->writeln('<error>Error creating user table: ' . mysqli_error($this->link) .'</error>');
						}
			} else {
				$this->output->writeln('<error>Error creating user table: ' . mysqli_error($this->link) .'</error>');
			}

		} else {
			$this->output->writeln('<error>No Database available.</error>');
		}
	}

	public function createDBandUserTable() {
		if($this->checkDbConnection() == false) {
			$sql = 'CREATE DATABASE '.$this->db . ' CHARACTER SET utf8 COLLATE utf8_general_ci';

			if (mysqli_query($this->link, $sql)) {
				$this->output->writeln('<info>Database '.$this->db.' created successfully</info>');
				$this->createUserTable();
			} else {
				$this->output->writeln('<error>Error creating database: ' . mysqli_error($this->link) .'</error>');
			}
		} else {
			$this->createUserTable();
		}
	}

	public function insertOneUser($user, $acceptedColumns) {
		$name = mysqli_real_escape_string($this->link, $user['name']);
		$surname = mysqli_real_escape_string($this->link, $user['surname']);
		$email = mysqli_real_escape_string($this->link, $user['email']);
		$sql = "INSERT INTO users ($acceptedColumns[0], $acceptedColumns[1], $acceptedColumns[2])
				VALUES ('$name', '$surname', '$email')";

				if (mysqli_query($this->link, $sql)) {
					$this->output->writeln('<info>User '.$user['name'].' created successfully</info>');
				} else {
					$this->output->writeln('<error>Error creating user '.$user['name'].' </error>');
				}

	}

	public function getLink() {
		return $this->link;
	}

}