<?php
require __DIR__.'/vendor/autoload.php';
require __DIR__.'/UsersUploadCommand.php';
 
use Symfony\Component\Console\Application;

$command =  new UsersUploadCommand();
$application = new Application('User Upload', '1.0.0');
$application->add($command);
$application->setDefaultCommand($command->getName());
$application->run();